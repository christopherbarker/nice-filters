# NICE test: Filters

- Candidate: Chris Barker
- Email: hellochrisbarker@gmail.com

## Notes

- This project was bootstrapped with Create React App for speed. In the real world (or with more time) I'd probably want to make a dev/build environment from the ground up, using Webpack or similar.
- The usual Create React App scripts apply: `npm start` to run the project, `npm test` to run the tests etc.
- The tests are very basic, currently just verifying that we're displaying the main element on the page and that the JSON is valid.
- The styling is *extremely* basic. I was contemplating bringing in an external CSS library to make things a little prettier, but decided that it wasn't a great use of the time. In the real world I'd be using SCSS with BEM, but here it didn't seem necessary.
- I'm far more familiar with Vue than with React, so there may be some odd code patterns here and there.
- I lifted the Prettier/ESLint config straight from the NICE component library repo.

## Things that I would have liked to do next:

- Break down the code into more components where appropriate, as App.js is a bit bloated
- Write tests for the Pagination component and any other new components that get created
- Make it look less basic
- Integrate SCSS into the dev pipeline
- Test properly with a screen reader
- Allow multiple filters to be combined
