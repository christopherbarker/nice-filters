import {React, useEffect, useState, useRef} from "react";
import Pagination from "./components/Pagination";

const PAGE_SIZE = 10,
	TYPES = {
		DOC: "ndt",
		ADVICE: "nat",
		GUIDANCE: "ngt"
	};

function App() {
	const [results, setResults] = useState([]);
	const [filteredResults, setFilteredResults] = useState([]);
	const [docTypes, setDocTypes] = useState([]);
	const [adviceTypes, setAdviceTypes] = useState([]);
	const [guidanceTypes, setGuidanceTypes] = useState([]);
	const [page, setPage] = useState(1);

	// Select list refs
	const adviceRef = useRef("adviceRef"),
		docRef = useRef("docRef"),
		guidanceRef = useRef("guidanceRef");

	useEffect(() => {
		const url = "./static/search-data.json";

		const fetchData = async () => {
			try {
				const response = await fetch(url);
				const json = await response.json();

				// Populate results
				setResults(json.documents);
				setFilteredResults(json.documents);

				// Populate filter arrays
				json.navigators.forEach(n => {
					switch (n.shortName) {
						case TYPES.DOC:
							setDocTypes(n.modifiers.map(m => m.displayName));
							break;
						case TYPES.ADVICE:
							setAdviceTypes(n.modifiers.map(m => m.displayName));
							break;
						case TYPES.GUIDANCE:
							setGuidanceTypes(n.modifiers.map(m => m.displayName));
							break;
						default:
							break;
					}
				});
			} catch (error) {
				console.log("Error fetching data:", error);
			}
		};

		fetchData();
	}, []);

	function handleFilter(type, select) {
		const value = select.current.value;
		setPage(1);

		if (value === "") {
			// Default option - just get the full result set
			setFilteredResults(results);
			docRef.current.value = "";
			adviceRef.current.value = "";
			guidanceRef.current.value = "";
		} else {
			// Filter by supplied value
			switch (type) {
				case TYPES.DOC:
					setFilteredResults(results.filter(r => {
						adviceRef.current.value = "";
						guidanceRef.current.value = "";
						return r.niceDocType.includes(value);
					}));
					break;
				case TYPES.ADVICE:
					docRef.current.value = "";
					guidanceRef.current.value = "";
					setFilteredResults(results.filter(r => {
						return r.niceAdviceType.includes(value);
					}));
					break;
				case TYPES.GUIDANCE:
					adviceRef.current.value = "";
					docRef.current.value = "";
					setFilteredResults(results.filter(r => {
						return r.niceGuidanceType.includes(value);
					}));
					break;
				default:
					break;
			}
		}
	}

	function handleNextPage() {
		setPage(page + 1);
	}

	function handlePreviousPage() {
		setPage(page - 1);
	}

	return (
		<div className="container">
			<header>
				<h1>NICE test: Filters</h1>
			</header>

			<main>
				<section>
					<h2>Filter your results</h2>

					<fieldset className="fieldset">
						<legend>Filter by document type</legend>
						<label className="label" htmlFor="docTypeFilter">Document types</label>
						<select className="select" id="docTypeFilter" ref={docRef}>
							<option value="">All document types</option>
							{
								docTypes.map((name, index) => (<option key={index}>{name}</option>))
							}
						</select>
						<button className="button" onClick={() => { handleFilter(TYPES.DOC, docRef); }}>Apply document type filter</button>
					</fieldset>

					<fieldset className="fieldset">
						<legend>Filter by advice type</legend>
						<label className="label" htmlFor="adviceTypeFilter">Advice types</label>
						<select className="select" id="adviceTypeFilter" ref={adviceRef}>
							<option value="">All advice types</option>
							{
								adviceTypes.map((name, index) => (<option key={index}>{name}</option>))
							}
						</select>
						<button className="button" onClick={() => { handleFilter(TYPES.ADVICE, adviceRef); }}>Apply advice type filter</button>
					</fieldset>

					<fieldset>
						<legend>Filter by guidance type</legend>
						<label className="label" htmlFor="guidanceTypeFilter">Guidance types</label>
						<select className="select" id="guidanceTypeFilter" ref={guidanceRef}>
							<option value="">All guidance types</option>
							{
								guidanceTypes.map((name, index) => (<option key={index}>{name}</option>))
							}
						</select>
						<button className="button" onClick={() => { handleFilter(TYPES.GUIDANCE, guidanceRef); }}>Apply guidance type filter</button>
					</fieldset>
				</section>

				<section>
					<h2>Results</h2>

					<Pagination
						previousPageDisabled={page < 2}
						nextPageDisabled={page * PAGE_SIZE >= filteredResults.length}
						handlePreviousPage={handlePreviousPage}
						handleNextPage={handleNextPage}
					/>

					<div aria-live="polite">
						{
							filteredResults.length > 0
								?
								<div>
									Showing results {(page-1) * PAGE_SIZE + 1} to {Math.min(page * PAGE_SIZE, filteredResults.length)} of {filteredResults.length}
									<div>Page {page} of {Math.ceil(filteredResults.length / PAGE_SIZE)}</div>
								</div>
								:
								<div>No results - please try changing your filters.</div>
						}

						<ul className="result-list">
							{
								pagedResults(page, filteredResults).map(r => (
									<li key={r.id}>
										{r.title}
										<div>Last updated: {new Date(r.lastUpdated).toLocaleDateString(undefined, {
											weekday: "long", year: "numeric", month: "long", day: "numeric"
										})}</div>
									</li>
								))
							}
						</ul>
					</div>

					<Pagination
						previousPageDisabled={page < 2}
						nextPageDisabled={page * PAGE_SIZE >= filteredResults.length}
						handlePreviousPage={handlePreviousPage}
						handleNextPage={handleNextPage}
					/>
				</section>
			</main>
		</div>
	);
}

function pagedResults(page, results) {
	const firstResultIndex = (page-1) * PAGE_SIZE;
	return results.slice(firstResultIndex, firstResultIndex + PAGE_SIZE);
}

export default App;
