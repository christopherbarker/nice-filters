import {React} from "react";
import PropTypes from "prop-types";

export default function Pagination(props) {
	return (
		<div className="pagination">
			<button className="button" onClick={props.handlePreviousPage} disabled={props.previousPageDisabled}>Previous page</button>
			<button className="button" onClick={props.handleNextPage} disabled={props.nextPageDisabled}>Next page</button>
		</div>
	);
}

Pagination.propTypes = {
	previousPageDisabled: PropTypes.bool,
	nextPageDisabled: PropTypes.bool,
	handlePreviousPage: PropTypes.func,
	handleNextPage: PropTypes.func,
};
