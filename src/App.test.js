import { React, render, screen } from "@testing-library/react";
import App from "./App";
import searchDataJSON from "../public/static/search-data.json";

test("page renders main element", () => {
	render(<App />);
	const mainElement = screen.getByRole("main");
	expect(mainElement).toBeInTheDocument();
});

test("Results JSON is a valid object", () => {
	expect(typeof searchDataJSON === "object");
});

test("Results JSON contains at least one document", () => {
	expect(searchDataJSON.documents.length > 0);
});
